# Developing CanESM from Version Control

### NB: These instructions are only relevant to development internal to CCCma. 

## Latest Updates
Last updated: NCS, 19/06/2019


## Introduction
This document describes the basic steps required to modify CanESM5/CanAM source code using the git Version Control System. 
The git `s` commands referred to below are special scripts available in `CCCma_tools/tools`.


## Modifying the code

1. **Create an issue**

    Before beggining work, an issue (under a milestone) should be created on the CCCma internal gitlab  
    [CanESM5 issue tracker](https://gitlab.science.gc.ca/CanESM/CanESM5/issues). This allows us to keep track of what was done 
    when and why. When setting up the issue, you should have a clear starting point (i.e. super-repo SHA1).<br><br>

2. **Clone the CanESM5 super-repo.**

    If you have setup a run on the XC40s, `setup-canesm` will have made a clone of the code
    and provided a link to it (`CanESM_source_link`), and you could modify the code directly there - the easiest approach. However, you may want to work 
    on you laptop or elsewhere, in which case see the notes below on cloning the repository.
    <br><br>

3. **Checkout named branches**

    You must checkout a new branch in the super repo, and in each submodule that you plan to modify. This is done automatically using "sbranch" and "scheckout".
    If a branch does not exist yet, then do (**from the super-repo level**):
    
        git scheckout -b BRANCH-NAME 

    if this does not work (we have seen issues in some versions of git used on lxwrk), you might try:
    
        git sbranch BRANCH-NAME
        git scheckout BRANCH-NAME
    
    where BRANCH-NAME is the name of your custom branch. If the BRANCH-NAME already exists, then do
    
        git scheckout BRANCH-NAME
    
    You can now go ahead and modify any code you like (in any submodule).<br><br>

4. **Add and commit the changes**<a name="adding_committing"></a>

    To add changes in all submodules and the super-repo automatically, use (from the top super-repo level, i.e. CanESM):
    
        git sadd
    
    you could optionally do custom staging by adding files manually with `git add file`. To commit you can use (from the top level):

        git scommit 
        
    This will require you to leave custom commit messages for each repo modified. <br><br>

5. **Push changes back to gitlab**<a name="pushing"></a>


    Again, from the super-repo level, push using:
    
        git spush origin BRANCH-NAME
<br>


6. **Launch a test run from gitlab**

    Lauch a test run from scratch using the modified code. To do this use your CanESM5 super repo commit obtained at the end of step 4 
    (you can use `git log` to look though recent commits, or look in the gitlab history to determine which commit you want to test).
    Follow the instructions in [Running_readme.md](Running_readme.md). 

       Iterate through 4-6 until you are satisfied that you changes are complete, and you want to hand them back for inclusion. At this
       point it is expected that you have a documenting run, done cleanly off a super-repo commit, which is stable.<br><br>
       
7. **Merge in `develop_canesm` and handle any conflicts.**

    Once you are satisfied with your changes, **you are responsible for confirming that your changes do not conflict with other work**
    **done to `develop_canesm`**. To do this, you must merge in `develop_canesm` into your development branch (see directions below),
    handle any conflicts, and commit/push the updated branch. If any _new_ changes were brought in via this merge, you should then 
    launch another test to make sure the code is still operational. 

8. **Create a merge request upon completion** 

    Once you have determined that your updates work with the `HEAD` of `develop_canesm` merged into your branch, you should then
    make a merge request (MR) on [gitlab](https://gitlab.science.gc.ca/CanESM/CanESM5), between your branch and `develop_canesm`. 
    As part of this MR, **you must select at least one** person to review your changes, and:
      1. explain what this MR is designed to fix/update
      2. provide the reviewer with a description of how you tested your changes
      
    Then upon approval of your changes, and a passing of a Continuous Integration (CI) pipeline, your code will be officially
    accepted and merged into `develop_canesm`.
    <br><br>

## Working with user "Forks"
As an effort to keep our main repository clean and reduce the chance of users incorrectly affecting others' branches, 
we have moved to a Forked workflow model for the majority of development. The forking workflow model is quite common in large collaborative
projects and there are many [resources](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow)
available online. However, in addition to these, we have also created a document 
[here](https://gitlab.science.gc.ca/CanESM/CanESM5/wikis/forking-workflow) to help our users through the transition
to this workflow in the CanESM framework. 

## Deleting old branches

To delete an old, no-longer used branch both locally and remotely use:

    git sdel BRANCH-NAME
    
be sure that no-one else is using it though.    

## Merging
**Note**: after May 29th, 2019, the main branch of the repository `develop_canesm` has been restructured. If you have a local
copy of the repository that was pulled down before this date, and you would like to update your branch with changes from `develop_canesm`, 
you will first need to follow the intructions laid out 
[here](https://gitlab.science.gc.ca/CanESM/CanESM5/wikis/restructuring-your-local-repo-to-match-the-develop-canesm-restructuring-(made-may-29th,-2019)).
After doing this, follow the directions below to update your branch.

Merging can be complex, and should not be taken lightly. If you need to merge in upstream changes to your branch, 
you can use `git smerge` to help. For example, if I want to add the latest develop_canesm updates to my branch fancy-test, I would do this:

    git sfetch                                     # Fetches latest updates from gitlab, for all submodules.
    git smerge origin/develop_canesm fancy-test    # merges origin/develop_canesm into fancy-test, across all submodules.
    
At this point you have to resolve any merge conflicts that arose. Then, do steps 4-6 above to complete the merge,
and push the updated branch back to gitlab, and test the final result straight off gitlab.

## Cloning the repository

To clone the CanESM5 code to you laptop or another location of your choice, use:

    git clone --recursive git@gitlab.science.gc.ca:CanESM/CanESM5.git 
    cd CanESM5 

CAUTION: This might fail if you have not setup your ssh keys on gitlab. Follow [these](https://wiki.cmc.ec.gc.ca/wiki/Subscribing_To_Gitlab) instructions.
At this stage you should checkout the starting point, e.g.:

    git checkout SHA1 

where the string following `checkout` is the hash (=SHA1=commit) or branch that you want to checkout. Be sure to update the submodules, e.g.:

    git submodule update --recursive --init --checkout

Now you are ready to modify the code and proceed. If you already have an established repo, just do the last two steps (checkout and submodule update)

## Additional Notes

## About git and version control

The CanESM5 version control system is based on git. If you know nothing about git these resources may help:

- [official git documentation](https://git-scm.com/doc)
- [Atlassian git tutorials](https://www.atlassian.com/git/tutorials)
- [Git sumodule docs](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
- [CCCma NEMO version control help page](http://wiki.cccma.ec.gc.ca/twiki/bin/view/Main/NemoVersionControl)

You should be comfortable with the git definitions of: "commit", "SHA1", "branch", "checked out", "staging", "commiting",
and "submodules" to work effectively with the CanESM5 code.



