Diagnostics, pooling and plotting for AGCM
====================

Running diagnostics
--------------------

Use the `make_diag_job` extracted by `setup-canesm`, to construct a diagnostic string 
(with built in xfer job), and submit it to ppp.

Modifying diagnostics
---------------------

To modify the diagnostics or to create new diagnostics, first make the changes in the CanDIAG
(following the instructions in [Developing_readme.md](Developing_readme.md) for changes to be contributed back).
Once the existing decks are modified, or new decks are created in `CanESM5/CanDIAG/diag4/`,
modify the `canesm_diag_decks` variable in `canesm.cfg` to update the
list of the diagnostics decks to be executed in the AGCM diagnostic
string. This variable is used by the `make_merged_diag_deck` script to
create a merged diagnostics deck in `$CCRNSRC/bin/merged_diag_deck.dk`
during the model setup stage. You will need to re-run the
`make_merged_diag_deck` script any time the list of decks
`canesm_diag_decks` in `canesm.cfg` is modified:

    # create the merged deck
    make_merge_diag_deck
