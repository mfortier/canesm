import sys
import os
from .util import get_test_ensembles, pause_for_file


sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble


def check_ensemble(ensemble_info):

    ens = ensemble_info.ensemble
    sha = ensemble_info.sha['output']['shas']
    sha_deltas = []

    # pause until run is finished
    job = ens.jobs[0]
    runpath = job.runpath.replace('work', 'sitestore')
    for s in sha:
        file = s['file'].replace('{runid}', job.runid)
        pause_for_file(job, file, directory=runpath)
        remote_sha = job.run_command(f'sha1sum {file}', setup_env=False, run_directory=runpath)
        remote_sha = remote_sha.stdout.strip().split()[0]
        if not s['sha'] == remote_sha:
            # store information about checksum change
            delta_inf = {
                    'file'      : file,
                    'old_sha'   : s['sha'],
                    'new_sha'   : remote_sha
                }
            sha_deltas.append(delta_inf)

    # if deltas were noted, print info and throw exception
    if sha_deltas:
        print("***********************************")
        print("     Checksum changes noted!       ")
        print("***********************************")
        for delta in sha_deltas:
            print("\tFile: {} \n\tOld Checksum: {} \n\t New Checksum: {}\n".format(
                            delta['file'],delta['old_sha'],delta['new_sha']))
        raise Exception("Changes in the output files!")

def test_ensemble():

    test_ensembles = get_test_ensembles()
    for ens_info in test_ensembles:
        check_ensemble(ens_info)
