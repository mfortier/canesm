import yaml
import os
import pandas as pd
from typing import List
from dataclasses import dataclass
import sys
import time
import re

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble, RemoteError

@dataclass
class EnsInfo:

    ensemble: CanESMensemble
    sha: pd.DataFrame


def generate_config_file(config_file: str) -> str:
    """
    update the configuration file to point to the current hash, with a unique runid and run_directory

    Parameters
    ----------
    config_file :
        path to the base config file

    """
    git_sha = os.environ['CI_BUILD_REF']  # gitlab <9 (CI_COMMIT_SHA if we ever upgrade)
    short_sha = git_sha[0:8]

    options = yaml.load(open(config_file), Loader=yaml.SafeLoader)
    options['ver'] = git_sha
    options['runid'] = f"{options['runid']}-{short_sha}"
    options['run_directory'] = os.path.join(options['run_directory'], short_sha)

    # get repo url so this can be done from user forks
    options['repo'] = os.environ['CI_BUILD_REPO']

    newfile = f'{config_file.split(".")[0]}_{short_sha}.yaml'
    yaml.dump(options, open(newfile, 'w'))

    return newfile


def get_test_ensembles() -> List[EnsInfo]:
    """
    according to the 'ensemble_tests.yml' file, determine what ensembles should be launched as part of the CI
    test, then create a suitable config file from the defaults in "config_files" and launch the ensemble using
    the ensemble tool
    """

    # get dictionary containing the "test_ensembles" 
    tests = yaml.load(open(os.path.join(os.path.dirname(__file__), 'ensemble_tests.yml')), Loader=yaml.SafeLoader)

    ens_info = []
    test_folder = os.path.dirname(__file__)
    for ensemble_name in tests['test_ensembles']:

        # get the default config files for this ensemble
        config_file = os.path.join(test_folder, 'config_files', f'{ensemble_name}.yaml')
        sha_file = os.path.join(test_folder, 'config_files', f'{ensemble_name}.sha')
        sha = yaml.load(open(sha_file), Loader=yaml.SafeLoader)

        # update cfg file's version, runid, and run directory and launch ensemble
        newfile = generate_config_file(config_file)
        ens = CanESMensemble.from_config_file(newfile)
        ens_info.append(EnsInfo(ensemble=ens, sha=sha))

    return ens_info


def delete_ensembles():

    ensembles = get_test_ensembles()

    for ensemble in ensembles:
        ens = ensemble.ensemble
        for job in ens.jobs:

            # delete files from ppp
            runpath = job.runpath.replace('work', 'sitestore')
            datapath = os.path.dirname(runpath)  # drop the /data folder
            job.run_command(f'chmod -R u+w {datapath}', setup_env=False)
            job.run_command(f'rm -rf {datapath}', setup_env=False)

            # delete .queue files
            job.run_command(f'rm -rf /home/{job.user}/.queue/*{job.runid}*', setup_env=False)

        # delete from backend
        ens.delete_ensemble()


def pause_for_file(job, file, directory: str = None) -> List[str]:
    """
    wait for files that match the file string to be created and return list of matching files
    """
    print("Waiting for {}".format(file),flush=True)

    if directory is None:
        directory = job.runpath

    not_found = True
    f = []
    while not_found:
        try:
            files = job.run_command('ls', setup_env=False, run_directory=directory, hide=True).stdout.strip().split('\n')
        except RemoteError:
            time.sleep(300)
            continue
        regex = re.compile(file.replace('{*}', '.*'))
        f = [string for string in files if re.match(regex, string)]
        if len(f) == 0:
            time.sleep(300)
        else:
            not_found = False

    return f
