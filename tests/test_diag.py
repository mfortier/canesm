import os
import shutil
import sys
from .util import get_test_ensembles, pause_for_file


sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble, CanESMsetup


def check_diag_files(ensemble_info):

    ens = ensemble_info.ensemble
    sha = ensemble_info.sha['diag']['files']

    # pause until run is finished
    job = ens.jobs[0]
    queue_dir = f'/home/{job.user}/.queue'
    stored_output_dir = os.path.join(queue_dir,".ci_failures")

    for s in sha:
        file = s.replace('{runid}', job.runid)
        file = pause_for_file(job, file, directory=queue_dir)
        for f in file:
            contents = job.load_file(f, directory=queue_dir)
            if not 'Job Successful' in contents:
                # store information on which stage failed
                shutil.move(os.path.join(queue_dir,f),os.path.join(stored_output_dir,f))
                
                # output failure information
                print("*****************************************")
                print("     A Diag job failed!                  ")
                print("*****************************************")
                print("\nSee {} in {}".format(f,stored_output_dir))
                raise Exception("{} failed!".format(f))

def test_diag_files():
    
    print("Checking diagnostic progress",flush=True)
    ensembles = get_test_ensembles()
    for ens_info in ensembles:
        check_diag_files(ens_info)
