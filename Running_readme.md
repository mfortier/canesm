# Running CanESM/CanAM from version control

### NB: These instructions only apply to ECCC's machines. No other machines are supported.

## Introduction

This document describes the five basic steps required to run CanESM5/CanAM from Version Control. If this is
your first run, see the section "One time setup" below. For instructions on how to modify the code see 
[Developing\_readme.md](Developing_readme.md). For more
information on the system and its rationale see [README.md](README.md) and
[twiki](http://wiki.cccma.ec.gc.ca/~acrnncs/CanESM5_development_guide.pdf).

### Runid Guidance
 
 As part creating a run, users must select a "runid", or run identifier, that will be used to differeniate
 their runs from others. In _general_ the chosen runids are free form, but there are _some_ restrictions,
 specifically, they must contain **only lower case alphanumeric characters [a-z] and [0-9], the hyphen "-" and the period "."**

## Setting up a run 

These steps are mostly the same for CanESM and CanAM AMIP runs, with differences noted below. Setup is done
on hare or brooks.

1. **Call `setup-canesm`**

    In the directory where you want to setup the run (normally ~/PRODUCTION/runid), call `setup-canesm`, specifying
    at a minimum the runid and version of the code to use, e.g. (CHANGE at least runid and ver!):

    AMIP:

        setup-canesm  ver=ABC config=AMIP runid=XXX repo=REPO_ADDRESS

    Coupled:

        setup-canesm  ver=ABC config=ESM runid=XXX repo=REPO_ADDRESS
   where
   - **`repo`:** _(optional)_ defines what repository the code should be cloned from, i.e. a user fork or the main
      repository - for example: `repo=git@gitlab.science.gc.ca:sci123/CanESM5.git` would tell 
      `setup-canesm` to clone the code from `sci123`'s user fork. **If the `repo` flag is not given**
      **`setup-canesm` defaults to using the [main repo](https://gitlab.science.gc.ca/CanESM/CanESM5), or**
      **`git@gitlab.science.gc.ca:CanESM/CanESM5.git`.** 
   - **`ver`**: defines the tag, commit/SHA1 checksum, or the branch from the CanESM "super repo" pointed to
     via the `repo` flag. (see Model versions section below);
   - **`config`**: is set to either `AMIP` for an Atmosphere run or `ESM` for a coupled run 
   - **`runid`**: is the unique runid. Do not re-use runids of existing runs, even setting up a run on a different user account! 
  
    **NB**: You must source the local environment after running setup-canesm: 

        . env_setup_file
    See Notes below for further details.<br><br>

2. **Create the job string**

    Edit the file `canesm.cfg`, to set dates and run options, as documented in that file, and then create the job string

        vi canesm.cfg
        ...              # Change start and end dates, etc. runid has been set already by setup-canesm

        ./make_job_XXX   # Create the job string by executing the make_job

    This creates a jobstring which is needed for compilation, and which is ultimately submitted to start the run. <br><br>

3. **Compile the executables and submit the job**

    Executables are compiled interactively by the user. `setup-canesm` will have extracted a file called
    `compile_XXX` (where XXX is the runid). Execute the compile script to do the compilation, specifying the model job as the first command line argument:

        ./compile_XXX XXX_2652m01_2656m12_job
    
    The compilation will take about 10 minutes. The executables are automatically saved with the correct name onto RUNPATH. See Notes below for further tips on fast compiling.

    The AGCM merged diagnostic deck is now created during the compilation step by executing the `make_merged_diag_deck` script. 
    The output merged diagnostic deck `merged_diag_deck.dk` is now placed in $CCRNSRC/bin/merged_diag_deck.dk, outside of the CanESM5 repository. <br><br>

4. **Save restarts to RUNPATH**

    `setup-canesm` will extract the script `save_restart_files_XXX`, which you can execute to save the default restarts (or modify as appropriate to save 
    restarts from another run on disk for use in the current run).

        save_restart_files_XXX XXX_2652m01_2656m12_job

    The save_restart_file script contains a hacker job to reset the count value in the restart file to correspond to model year.
    Alternatively, use the `tapeload_rs` script to load restart files for a given runid/year off tape. The jobstring is provided as an argument. <br><br>


5. **Submit the job.**

        rsub MACH $MODEL_JOB

    where `MACH` is currently either `hare` or `brooks`. If you want to submit the job to be submitted automatically after compilation finishes, do the following:

        ./compile_XXX XXX_2652m01_2656m12_job ; ./save_restart_files_XXX ; rsub brooks XXX_2652m01_2656m12_job
        
    (you can also enter "rsub jobstring brooks" or "rsub jobstring br" instead of rsub brooks jobstring (br is abbreviation for brooks, ha for hare, ba for banting and dy for daley))
    
    Note that you MUST have sourced the correct environment (see step 1) to have access to rsub. <br><br>

## Continuing an existing run

### Run has crashed before its scheduled end

If a run crashes, the normal recovery procedure is to try and resubmit the model jobstring from the `.crawork` directory, i.e.:

    cd ~/.crawork
    ls -lrt | grep $runid # look for a file containing modljob
    rsub MACH JOB
    
where `MACH` is hare or brooks (as indicated in the job name) and `JOB` is the jobstring identified on the second line.

### Run has reached the end of it's jobstring

  To continue a run which has run out of string, modify the dates in the `canesm.cfg` file, create a new jobstring and
  submit this.

  CAUTION:
  One thing to be aware of is that you must ensure you are in the correct environment for your runid. That
  is, you should source the `env_setup_file` for your run   (as in step 1 above). A quick check is to do `which rsub`.
  
## Model versions

The branch `develop_canesm` is used to integrate in new changes, and always reflects the lastest developments. We
strive to keep `develop_canesm` stable and even bit-identical to the previous tagged release, but issues can arise.

When important changes occur, a tagged release is issued. Tagged releases are thoroughly tested, stable versions
of the model (although old taggaed releases might not function as HPC changes). The latest tagged release should
always be functional and stable, and represents a reliable starting point for work.
    
To find the latest tagged release, visit [CanESM5 repository on gitlab](https://gitlab.science.gc.ca/CanESM/CanESM5). 
From the top horizontal menu bar, select 'repository'. Then from the secondary menu bar, select 'Tags'. The latest tagged
release is listed at the top, with its commit number underneath. Use that for 'ver=' in the call to setup-canesm."

When new tagged releases are issued, users should merge these into their working branches ASAP.

## One time setup

You do require access to one script (setup-canesm). To get this modify `~/.profile` to include the line:

    export PATH=/home/scrd101/canesm_bin_latest:$PATH

(and make sure to log back in or to source ~/.profile). It is suggested that every time you use an account to
start or continue a run, you first verify that "~/.profile" is clean.

You must also make sure that the account that you are using has its SSH keys added to gitlab. If you get prompted
for a password during cloning/setup then this has not been done. Follow
[these](https://wiki.cmc.ec.gc.ca/wiki/Subscribing_To_Gitlab) instructions to add your keys.

Conflicts in your environment might cause problems. If things are not working as expected, try again with a "clean"
environment. By clean we mean that `~/.profile` does not source the CCCma environment (and preferably, should be nearly empty). The line

    . /home/scrd101/generic/sc_cccma_setup_profile

which appears by default in CCCma user accounts should be commented out to make the environment clean (and log back out/in again).

## Notes

### Note on setup-canesm

Your call to `setup-canesm` will:

  - clone the source code (for ver) to a place called RUNID_ROOT, in /home/ords/crd/ccrn/$USER/$runid.
  - extract a reference `canesm.cfg` for the user to edit as they need (for both the model and diagnostics).
  - extract a `make_job_$runid`, used to generate the job string. Also `make_diag_job` and `make_dump_job`, which the user does not need to call (done automatically)
  - extract a compilation script for the user to compile the source code.
  - create soft link to the CanESM source code in RUNID_ROOT (`CanESM_source_link`)
  - append an entry to .setup-canesm.log, detailing what you have done.

For more help and options see:

    setup-canesm -h

REMEMBER: source the env\_setup\_file as instructed by `setup-canesm` to get an interactive environment:

    . env_setup_file

If you ever log-off or start up a new terminal, you need to be sure to source this file. This may seem onerous, but
it is being done to try and ensure that each run in self-contained and repeatable.


NOTE: `setup-canesm` will optionally compile, save restarts and submit the job for you (see options -a, -c)

CAUTION: you can specify a branch as a `ver`. This is somewhat dangerous to reproducibility, because branches change in time.
         By using a SHA1 checksum, you are guaranteed to be able to use the exact same code in the future.


### Tips for compiling

### AGCM
When debugging AGCM code, it is useful to go directly to the build directory

    cd /home/ords/crd/ccrn/$USER/$runid/code/build/agcm/build/

There should be a makefile created by the compilation script. After modifying your code, you can do the following:

    release mc_${runid}_${rsyear}_m12_ab    # remove the existing model binary, if it exists
    make                                    # create a new model binary
    save mc_${runid}_${rsyear}_m12_ab mc_${runid}_${rsyear}_m12_ab # save the binary in $RUNPATH
    release mc_${runid}_${rsyear}_m12_ab    # clean up again

If only a few subroutines are modified, the compilation process should take much less time.
When testing compilation flags, they can be modified in `makefile` (search for FFLAGS variable).
Please note that after changing the compilation flags, a clean recompile may be required:

    make clean

After that, repeat the above steps.

### NEMO

Note that NEMO does an "in source" compilation. If you make a change to NEMO, only the change and it's dependencies
will be recompiled.

