Advanced topics
===============

.. contents:: :local:

Low level configuration
+++++++++++++++++++++++

Compilation
-----------

**CanAM and CanCPL**

* Configuration controlled by cpp keys in `cppdef_config.h`
* Some model size settings, including number of MPI tasks to use, resolution
  and number of tracers controlled in `cppdef_sizes.h`.

**NEMO**

* Controlled by source components specified in the `nemo/CONFIG/cfg.txt` file.
* cpp keys controlled by the `cpp*.fcm` file in the relevant CONFIG directory.

  

Controlling model timers
------------------------

CanAM and NEMO each control their own timers, and CanCPL also tracks time.
The total runtime must be configured consistently across components. In addition,
each component timestep must divide equally into a coupling cycle.

**CanAM**

* Start timestep is controlled by `kstart`, which comes in from the restart (`RS`)
* End timestep is controlled by `kfinal`, which is specified as one of the formatted
  reads at the top of the `modl.dat` namelist.
* Timestep is specified by `delT`. In general, the model iteration in steps from year
  1.

**NEMO**

* Start timestep is controlled by `nn_it000` in the `namelist`.
* Stop timestep is controlled by `nn_itend` in the `namelist`.
* Timestep is controlled by `nn_rdt` in the `namelist`.

**CanCPL**

* Some concept of time in the env file?
* Coupling frequency is specified by `ksteps` (where?). Coupling will actually happen
  whenever the NEMO and CanAM timsteps divide evenly into this.  


MPI topology
------------

**CanAM**

* Controlled at compile time by `_PAR_NNODE_A` in `cppdef_sizes.h`. This must match number of tasks provided
  at runtime. Openmp generally controlled at runtime by the `OMP_NUM_THREADS` environment variable. When running
  the coupled model, CanAM MUST appear first, because its master task insists on being task zero. You cannot
  put CanCPL or NEMO before CanAM on the mpirun etc call.

**NEMO**

* Controlled at runtime by the `jpni`, `jpni` and `jpnij` variables in `namelist`.


Coupling and model exchanges
++++++++++++++++++++++++++++

**CanAM**

In CanAM, `coupler_in` and `coupler_out` subroutines (actually live in `CanCPL/src/cpl_agcm.F90`), generally
control the flow of information between CanAM and CanCPL.

**CanNEMO**
In NEMO, the nemo coupling namelist specifies which fields are exchanged, and it chooses between various
NEMO options for intepreting these field (e.g. conservative). This is distinct from the remapping type in
the coupler.

**CanCPL**
There are various lists in the coupler which must be modified to exchange a new field.

The coupler specifies the type of remapping operation performed (e.g. conservative vs bilinear etc).


Building configurations with different resolution
+++++++++++++++++++++++++++++++++++++++++++++++++


