CanESM Version Control System
=============================

.. note::

    this document is not meant to replace the vast amount of ``git``
    documentation that exists online. ``git`` is an extensive, yet well
    documented tool that is utilized by millions of people a day. As a result, many of
    the questions and problems users may run into have already been asked and
    solved online.

Due to the complexities and size of the ``CanESM`` code-base, combined with the collaborative 
nature of the respository, interacting with and contributing to it requires users and developers
to be aware of some additional ``git`` topics, over and above basic concepts. This document aims to 
provide guidance in this regard, which specific focus on applying them to ``CanESM``. 

- :ref:`The Forking Workflow <Forking Workflow>`
- :ref:`Working with Submodules <Working with Submodules>`
- :ref:`CanESM VCS Tutorials and Screencasts <CanESM VCS Tutorials and Screencasts>`

Git Resources and Important Terms 
---------------------------------
In order to work with the ``CanESM`` code-base effectively, it the resposibility of developers to have at least a
working understanding of various ``git`` concepts. Specifically, developers should be comfortable with the following
terms:

- "commit" or "SHA1"
- "branch"
- "checked out"
- "staging"
- "commiting"
- "merging"
- "conflicts"
- "submodules" 
- "remotes"

If developers find themselves struggling with these concepts, they should take the time to get up 
to speed by working through external documentation, such as (but not limited to):

- `Official git documentation <https://git-scm.com/doc>`_
- `Atlassian git tutorials <https://www.atlassian.com/git/tutorials>`_
- `Git - submodule docs <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_
- `Git - working with remotes <https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes>`_
- `Atlassian - git merge conflicts <https://www.atlassian.com/git/tutorials/using-branches/merge-conflicts>`_
- `Github - resolving merge conflicts from the command line <https://docs.github.com/en/free-pro-team@latest/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line>`_

Forking Workflow
----------------
One of the key things contributers should be aware of is that to keep the
"central" repository clean and provide developers a more isolated workspace,
the ``CanESM`` development workflow uses a forking model, or a "Forking Workflow".
In addition to keeping the central repository cleaner and more controlled, this
workflow also brings ``CanESM`` development more inline with open source
development methods.

For developers who are not familiar with this workflow, they are encouraged to take
a look at the following documentation (or other external resources):

- `Atlassian - Forking Workflows <https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow>`_
- `Medium - Git Forking Workflow <https://medium.com/dev-genius/git-forking-workflow-bbba0226d39c>`_

Of particular note, readers are directed to the `Atlassian documentation
<https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow>`_,
as it provides a detailed, yet parseable description. 

While forking workflows are quite common and described online in many places, we provide here
a high level summary of the concepts, and outline how they are specifically applied
in the ``CanESM`` system.

What is an Forking Workflow?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Prior to discussing what a "fork" is, it is worth laying out what a "remote"
repository is compared to a local one. For most of us, the repos we
typically interact with are on-disk or "local" repositories which we can
navigate to and manually modify. In contrast, a remote respository (hereafter
referred to as a "remote"), as laid out in the ``git``
`documentation <https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes>`_, 
is a version of your repo that exists *somewhere else* which (often) has stricter read/write
permissions. Typically, these remotes are hosted on an internet server, such as:

- `gitlab.com <gitlab.com>`_
- `github.com <github.com>`_
- `gitlab.science.gc.ca <gitlab.com>`_ - *internal ECCC employees only*
- `bitbucket.com <bitbucket.com>`_

but they can also be stored on local servers. Regardless of where they are
located, developers typically work with their own local repos and then
push/pull changes to and from remotes in order to share changes. 

Now, **in a non-forking workflow there is typically only one remote
repository** which developers clone from and push/pull changes to and from.
This type of workflow is illustrated below:

.. figure:: /images/Single_Remote_Workflow.png
    :width: 600
    :align: center
    :alt: Non-Forking Workflow Schematic

    Single remote (non-forking) workflow

Under this non-forking workflow, it is easy for a team of developers to share
their code through one remote, however, this model can break down when:

- the number of development branches and developers gets large or
- external developers wish to contribute to the code-base.

The first condition can result in a large number of orphaned branches in the
central repo, or worse, lead to one developer overwritting/deleting a
colleague's changes. The second condition causes problems because it requires
explicitly giving external developers access to your central repository, but
depending on the code base and location of the remote, that may not be advised nor
possible. 

To avoid these problems, we tag in the forking workflow. In summary, what this
means is that instead of relying on a single remote, **each developer "forks"
the central repo, creating a "server-side" clone of it**, which results in 
many developer specific remotes. Under this workflow, developers push changes
to their fork, and when they want their changes integrated into the central
repository, they make a merge request across remotes (from their remote into
the central one). A schematic of this workflow is shown below:

.. figure:: /images/Forking_Workflow.png
    :width: 700
    :align: center
    :alt: Forking Workflow Schematic

    Forking workflow 

Now, it goes without saying that visually, this information flow looks notably more 
complex than the single remote workflow. However, practically, the differences
experienced by the developer are minor, with the main differences being that:

- developers need to be aware of what remote they are interacting with, and
- how to update their fork when the central repo is updated

On a day to day basis, developers will typically still only interact with **one** 
remote (their personal fork), and only need to interact with the central repo 
when making merge requests or pulling in changes.

Working with CanESM Forks
^^^^^^^^^^^^^^^^^^^^^^^^^

In working with forks in the ``CanESM`` context, there are a few things to note:

1. Each developer must now fork the ``CanESM`` source code in order to contribute. The central repository
   has had its permissions severly restricted so only a small number of people will have access 
   to it - **most developers will not be able to push changes to the central repo**
2. For developers who wish to contribute to ``CanESM``, **they must make merge requests from their personal forks**. 
3. Periodically, **developers will need to manually update** ``develop_canesm`` **on their fork**.  
4. User forks are to be used for development. **All production runs MUST continue to occur 
   from clones/setups off the central repo**.
5. Issues should continue to be posted on the central repository.

For more specific, pratical guidance, developers are directed to the following links:

.. toctree::
    :maxdepth: 2

    canesm-forks.rst 

Working with Submodules
-----------------------
As laid out :ref:`here <Code Structure>`, the following components of the ``CanESM`` source code
are tracked as ``git`` submodules: ``CanAM``, ``CanDIAG``, ``CanCPL``, ``CanNEMO``, and ``CCCma_tools``.
Developers who are unfamiliar with submodules, are encouraged to spend some time getting 
familiar with the concept through external documentation, such as (*but not limited to*):

- `Official git submodule documentation <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_
- `Atlassian git submodule documentation <https://www.atlassian.com/git/tutorials/git-submodule>`_
- `Using submodules in git - tutorial <https://www.vogella.com/tutorials/GitSubmodules/article.html>`_
- `Working with submodules - blog <https://github.blog/2016-02-01-working-with-submodules/>`_

What are Submodules?
^^^^^^^^^^^^^^^^^^^^
In summary a "submodule" is just a nested repository. To pratically
understand what this means, a visual example is useful. Specifically, imagine we
have a repository with the title ``Project``, which contains some
miscellaneous files along with two components; in a version of this repo that
*doesn't use submodules* (hereafter refered to as a "Monorepo"), ``git`` will 
be tracking it as 

.. figure:: /images/MonoRepo.png
    :align: center
    :alt: Monorepo Example

    Monorepo Example

Specifically, this means that ``Project`` tracks *all* changes in
``component1`` and ``component2`` - for example, if ``file3`` is modified, when

.. code-block:: text

    >> git status

is ran **anywhere** within ``Project``, we see

.. code-block:: text

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

        	modified:   component1/file3

Now, provided the code for ``component1`` and ``component2`` remains relatively
simple, this is a perfectly valid way to track this project. However, when the
complexity of ``component1`` and ``component2`` baloons to a point where the
developers want to track the development of sub-components separately (e.g. for
an Atmosphere or Ocean model), this is where submodules come in! Using submodules
to track ``component1`` and ``component2`` just means that we now treat them as
their *own* repositories, and ``Project`` now only keeps track of the *commit* of 
each of these components, i.e. ``git`` will be tracking it has 3 *separate* repositories:

.. figure:: /images/SuperRepo.png
    :align: center
    :alt: Super-repo Example

    Super-repo Example

To see how this differs from the monorepo example, if we decide to modify
``file3`` again and run ``git status`` directly beneath ``Project``, we see:

.. code-block:: text

	rcs001@hpcr4-in:~/TMP/Project
	 >> git status
	On branch main
	Changes not staged for commit:
	  (use "git add <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)
	  (commit or discard the untracked or modified content in submodules)

			modified:   component1 (modified content)

and if we also *add* a new file (``file6``) to ``component1``, the "``modified``" line becomes:

.. code-block:: text

    modified: 	component1 (modified content, untracked content)

The "Super-repo" ``Project`` only knows that there has been
modifications/additions in ``component1``, but it doesn't know (or care) about
the specifics - one needs to navigate *into* ``component1`` to see the
specific changes. 

It is worth noting that if the developer wishes to commit the
changes in ``component1`` and have this *new version* be used on the ``main``
branch of ``Project``, they would first need to ``git add/commit`` the changes
within ``component1``, and then navigate back up to ``Project`` and ``git add/commit``
the new version of the sub-component. For the sake of being comprehensive,
it should be stated that when changes are commited in a submodule, ``git
status`` at the super-repo level will produce a "``new commits``" message, i.e:

.. code-block:: 

    modified:   component1 (new commits)

and to commit the new version to the super-repo, the developer then needs to run

.. code-block::

    git add component1

like it is an individual file. 

Interacting With the CanESM Submodules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When working with the ``CanESM`` source code, developers should be aware of the following:

1. To clone to super-repo **and** the submodules in one command, developers should use:

   .. code-block:: text

        git clone --recurisve [Git url]

2. To aid developers and maintainers in executing merges across all components, 
   along with tracking *what versions* of the sub-components work with each other, feature branches
   should be created across **all** repositories (i.e. your branch ``feature1`` should exist in 
   each repository).

The S-Scripts
"""""""""""""
Now, with the above stated, developers/contributers are obviously able to
interact with the super-repo and submodules using raw ``git`` comands, however,
to aid in working across the various submodules, the technical development
team has created a suite of ``git-s*`` scripts (or "*s-scripts*"), that are
version controlled `here
<https://gitlab.com/cccma/cccma_tools/-/tree/develop_canesm/tools>`_.

To gain access them, you can manually add the path to them to your ``PATH``
variable, or if operating on the internal ECCC machines, you can run:

.. code-block::

    . /home/scrd102/generic/sc_cccma_setup_profile

which setups the "CCCma Environment". Once done you should be able to 
utilize the following helper scripts.

- ``git sbranch ARGS``:
    
    used to execute ``git branch ARGS`` across all repos
- ``git scheckout ARGS``:

    used to execute ``git checkout ARGS`` across all repos
- ``git sadd``:

    used to execute ``git add -A`` across all repos, which stages all *possible* changes. 
    
    As discussed :ref:`here <Making Changes>`, when used in combination with ``git scommit``, 
    can be used to easily stage/commit all changes across the repositories.

- ``git scommit ARGS``:

    used to execute ``git commit -a ARGS`` across all repos. 
    
    As discussed :ref:`here <Making Changes>`, when used in combination with
    ``git sadd``, can be used to easily stage/commit all changes across the
    repositories.

- ``git spush remote-name branch-name``:

    used to execute ``git push remote-name branch-name`` across all repos.

- ``git sfetch ARGS``:

    used to execute ``git fetch ARGS`` across all repos.

- ``git smerge branch1 branch2``:

    used to merge ``branch1`` **into** ``branch2`` across all repos. 

    **Note**: before merging this tool checkouts ``branch2``.

- ``git sdel branch-name``:
    
    used to delete branch ``branch-name`` across all repos locally and attempts to delete
    remote versions on ``origin`` (the default remote)

- ``git sfed ARGS``:

    executes ``ARGS`` (which could be another command) across all submodules, taking
    a bottom up approach. 

    **Note**: this script is mainly used within other *s-scripts*.

- ``git sremote username <remote-name>``:

    used to add a new remote for all repos, where it assumes that the new
    remote url follows the same structure as that of the *existing* remote 
    urls in the project (i.e. ``git@gitlab.com:user123/canesm.git`` 
    or ``git@gitlab.science.gc.ca:user123/CanESM5.git``), but switches 
    out the existing user/group-name with ``username``. If the optional
    argument ``remote-name`` is given, ``git sremote`` sets it to be the new
    remote's name, else it makes the name ``username``.

    As an example, lets say we have super-repo with the following remote setup: 

    .. code-block:: text

        >> git remote -v
        origin  git@gitlab.com:cseinen/canesm.git (fetch)
        origin  git@gitlab.com:cseinen/canesm.git (push)

    .. note::

       for developers who cloned from the internal ECCC ``gitlab`` server,
       the remote url would look like:

       .. code-block:: text

            git@gitlab.science.gc.ca:user123/CanESM5.git

    Then, by executing:

    .. code-block:: text

        >> # at the super repo level
        >> git sremote nswart remote2

    ``git sremote`` would add remotes for *each* repo with urls that look like:

    .. code-block:: text

        git@gitlab.com:nswart/${REPO_NAME}.git
        
    where ``${REPO_NAME}`` is replaced with the project name of the repo (i.e.
    ``canesm``, or ``candiag``). ``git sremote`` would also set the remote-name
    to ``remote2``, so the output of ``git remote -v`` would look like:

    .. code-block:: text

        >> git remote -v
        origin  git@gitlab.com:cseinen/canesm.git (fetch)
        origin  git@gitlab.com:cseinen/canesm.git (push)
        remote2 git@gitlab.com:nswart/canesm.git (fetch)
        remote2 git@gitlab.com:nswart/canesm.git (push)

    If the second argument was not provided, ``remote2`` would be replaced
    with the ``username`` argument, which was ``nswart``.

    .. note::

        for developers using the internal ECCC ``gitlab`` server, the
        new url would look like:

        .. code-block:: text

            git@gitlab.science.gc.ca:nswart/CanESM5.git


CanESM VCS Tutorials and Screencasts
------------------------------------
To aid contributors in working with the ``CanESM`` version control system, the technical development
team has prepared the following screencasts tutorials:

- Overview of the ``CanESM`` repo structure and philosophy
- Establishing on disk repositories for the ``CanESM`` source code
- Making and submitting code changes to the ``CanESM`` source code

   - Making changes to the code
   - Submitting a merge request
- Merges, conflicts, and error handling strategies
