# Running CanESM on Compute Canada / Cedar (Singularity)

## Overview and resources

This document provides instructions for running the CanESM5 model on the Compute Canada machine *Cedar*. It is an opinionated guide that suggests a recommended approach. These instructions are likely relevant to other Compute Canada systems, but it has not been tested.

To enable external use of the model, CanESM5 has been ported to compile with the GNU compiler, and a Docker container established. At 
the time of writing, the relevant CanESM5 branch is `gcc-compile`. 

Working on Cedar requires converting the CanESM Docker container to singularity, and also interacting with the SLURM workload 
manage, as well as several other customizations to sequencing required to run on Compute Canada. 

We are going to use the space `~/project/$USER` to store some persistent data, and we'll put some important files in `$HOME`. You may changes this if you so desire (e.g. to `~/scratch`, but note that this space is temporary and periodically deleted).

For setting up a model run, we are going to use the `~/scratch` space, which is temporary space that can handle lots of data (note, data here will be deleted after 60 days or so). For CanESM, we often use the concept of a `RUNID` as a unique identifier of an individual run. We normally create a dedicated directory to work in for each `RUNID`.

**NB: Only a small number of model configurations are made available. There is no support whatsoever available for using CanESM. We do not certify that
the code runs correctly. This is strictly an alpha testing project with no guarantees whatsoever.**

## Basic requirements

### Get the source code

- Recursively clone the CanESM source code and checkout the `gcc-compile` branch. We'll put the source code in our home directory as it is small, we want to keep it permanently, and home has relatively fast access.

```
cd ~/
git clone --recursive https://gitlab.com/cccma/canesm
cd canesm

# Use a utility script to checkout the same branch across submodules
CCCma_tools/tools/git-scheckout gcc-compile
```

- Note, you might want to clone from `git@gitlab.com:cccma/canesm.git` instead. This will avoid having to enter your password on every push. It requires setting up SSH keys and entering your public SSH key into gitlab ([see here](https://docs.gitlab.com/ee/ssh/)). 

- You may wish to modify your path variable to include the locations of useful CanESM scripts. For example in `~/.bashrc` you can set path:

```
export PATH=$PATH:$HOME/canesm/CCCma_tools/tools:$HOME/canesm/CCCma_tools/tools/container_tools
```

- this will mean utilities like the git `s` commands (see `scheckout` above), and other utility scripts are available.

### Build the singularity image (once)

- The model is compiled and run inside a Docker or Singularity container, which provides all the required dependencies. Using the container alleviates the need to port the model dependencies to each individual HPC system. It is in principle possible to compile the model on the
native HPC. However, then all dependencies must be met, including the ESMF library. We do not provide instructions for this, and assume use of the container. Singularity is available on Compute Canada machines, is designed for HPC, and does not significantly degrade performance relative to bare metal.

The first step is to build the singularity image from the public Docker Hub image that we provide.  In order to build the container, you should create an interactive session on a compute-node first. We'll build the image in an interactive session, and since it is large we will save it in the project space.

```
# start an interactive session and go to a tmpdir 
salloc --time=1:0:0 --constraint=cascade --nodes=1 --ntasks-per-node=1 --mem-per-cpu=5000
cd $SLURM_TMPDIR

# Build the singualrity image from the public Docker hub image
singularity build canesm-docker_latest.sif docker://swartn/canesm-docker

# Create a director to store the image for future use
mkdir -p ~/project/$USER/singularity_images
mv canesm-docker_latest.sif ~/project/$USER/singularity_images
exit
```

- You only need to build the image once. From then on you can simply use this image. (You might also be able to get a version
of the image from existing users).


### Download the Basic required input data

- The basic input data needed to run CanESM is available on FTP for some selected experiments. You need to download the data
  for the experiment that you want to run. We typically use standard, CMIP define experiment names. The example below is for
  the `piControl` experiment. This data also contains a default restart file.

```
cd ~/project/$USER
mkdir -p canesm_input_data/piControl
cd canesm_input_data/piControl
wget ftp://ftp.cccma.ec.gc.ca/pub/CCCMA/nswart/canesm5_piControl_config/*
```

## Compiling the source code

### Compilation settings

Any required compilation time settings must be applied before compilation. These might include setting the correct cpp keys for the 
experiment, and setting the correct model sizes. 

#### MPI size of CanAM
- The number of MPI tasks to be used for CanAM must currently be specified at compile time. This value  is set in `canesm/CanAM/build/cppdef_sizes.h` using the variable `_PAR_NNODE_A`. The default value in testing on Cedar was `16`. Note this specifies the number of MPI tasks - but CanAM also uses openmp, so this is not the total number of cores. The total number of cores used is `_PAR_NNODE_A x OMP_NUM_THREADS`, where
`OMP_NUM_THREADS` is specified in the runtime script (see below). Also note, the number of MPI tasks specified must match with the resources supplied at runtime. 

#### Model / experiment configuration of CanAM

The `canesm/CanAM/build/cppdef_config.h` file defines the model configuration being used for CanAM. This varies between experiments. The default `cppdef` files are for the piControl experiment. The nominal procedure is simply to replace the contents of `cppdef_config.h` with that from the relevant file. Various different `cppdef` files can be found in `canesm/CONFIG/ESM/cppdefs` or `canesm/CONFIG/AMIP/cppdefs`, and these can be copied into `canesm/CanAM/build/cppdef_config.h` to configure a different experiment. Some experiments might require modification of `canesm/CanAM/build/cppdef_sizes.h` (e.g. free CO2 experiments), however this is less common, and no current guidance exists for this. Further work will be done to enable more easily selecting different experiment types (Note, within ECCC machines, cppdefs are typically specified by the `runmode` construct, and the sizes are computed and set by the compilation script. It might be helpful looking in `canesm/CONFIG/ESM/canesm.cfg` to determine the correct `cppdef` file to use for an experiment).

### Batch compile the model using utility scripts

Some utility scripts have been developed to assist with compiling the model. These are examples, not infallible systems. The script
`canesm/CCCma_tools/tools/container_tools/batch_compile_cedar` will compile the model automatically as a batch job, and return the
executable for later use. Some basic information must be provided. This information must be supplied in the file `canesm.cfg`. An example
of this file can be found at `canesm/CCCma_tools/tools/container_tools/canesm.cfg`. You must edit the paths in this file to reflect the location
where you have put your code, etc. On Cedar, batch jobs must be launched from `~/scratch`. The following shows an example of launching the
batch compilation. Note we use a directory called `canesm-test-run`. This is the RUNID we are using for this run, and this RUNID appears in the `canesm.cfg` configuration file. We can change the RUNID, but the paths in the configuration file must be adjusted accordingly.

```
# Go to the scratch space and make a directory to work in
cd ~/scratch
mkdir canesm-test-run   # Note this "runid" is arbitrary. You can use another name.
cd canesm-test-run

# Copy in the config file, and edit the paths to point to the correct places. 
cp ~/canesm/CCCma_tools/tools/container_tools/canesm.cfg .

# Launch the bash compilation script
sbatch ~/canesm/CCCma_tools/tools/container_tools/batch_compile_cedar
```

- You can monitor the progress of this job with `sq` command. Upon compeletion, we should expect to see in
 `~/scratch/canesm-test-run/bin` the three model executables:

 ```
 AGCM
 CPL
 nemo.exe
 ```

### Interactively compile the source code manually

Compiling the source code manually requires launching an interactive session, running the singularity container, and then compiling and saving the model executables. Since we need to singularity container to meet the dependencies, it is generally not possible to compile Interactively
on the cedar headnodes. Note, users may attempt to compile the model on the native machine with the container (but this requires compiling all the dependencies including ESMF).

- Launch an interactive session, move to a tmp directory, and launch an interactive singularity container from the image built earlier:

```
salloc --time=1:0:0 --constraint=cascade --nodes=1 --ntasks-per-node=8 --mem-per-cpu=5000
cd $SLURM_TMPDIR
singularity shell --cleanenv -B /home -B /project -B /scratch -B /localscratch ~/project/$USER/singularity_images/canesm-docker_latest.sif
```

- Copy in the source code and compile the model using the provided utility script:

```
# Alternatively, reclone and scheckout gcc-compile. It might be faster given how slow ~/project is!
cp -rp ~/canesm . 
 
# Compile the code with the utility script provided. See the script for individual steps. 
canesm/CCCma_tools/tools/container_tools/compile_cedar
```

- Remember to copy the executables for later usage, because the $SLURM_TMPDIR will be deleted after the session ends. e.g.

```
mkdir -p ~/project/$USER/canesm_exes/
cp AGCM CPL nemo.exe ~/project/$USER/canesm_exes/

# exit the container & the interactive session
exit ; exit 
```

## Running a job

The runtime configuration of the model must match the compilation options, input data, and so on. The model is run in batch mode, within the singularity container. Some basic runtime configuration is set in the `canesm.cfg` file (including relevant paths for binaries, input data, outputs, etc). This section assumes you have edited the `canesm.cfg` file appropriately.

### Configuring simulation length

The specified length of the simulation must match between CanNEMO and CanAM. 

- The number of CanNEMO steps is specified in the NEMO `namelist` file (found in the input directory). For CanESM5, the nemo timestep is 1 hour, and the number of steps is specified in hours (e.g. 8760 steps for 1 year). 

- In the CanAM, the timers are configured both by the date in the restart (known as kstart), and by the `kfinal` value given in the namelist file `modl.dat`. The default restart `kstart` value is `194436960` (which corresponds to 31 December 5549). In CanESM the CanAM timestep is 15 minutes. The desired `kfinal` value can be set in modl.dat as `4 x 24 x 365 x year_end`, where `year_end` corresponds to the end date of the simulation (shorter periods can also be run, but again, the number of steps must be consistent between NEMO and CanAM.)

- Note in CanESM5, CanAM takes 4 steps (15 m each) for each single NEMO timestep (1hr each). Coupling occurs every three hours, which is 3 NEMO timesteps and 12 CanAM steps. Below we rely on the default values having been set correctly. If the timers in model.dat and namelist are not set correctly, the run with not work.


### Launching a batch run

- Launch the run in a batch session (batch jobs must be launched from `~/scratch`):

```
   cd ~/scratch/canesm-test-run

   # Assume that canesm.cfg is here, and the paths are configured appropriately. The executables must be pre-compiled and available in
   # $BIN_DIR

   # If needed, copy this file and modify it for your case. Submit to the queue with `sbatch`.
   sbatch ~/canesm/CCCma_tools/tools/container_tools/batch_run_cedar
```

- The outputs will appear in the location defined in `canesm.cfg` by thge variable `OUTPUT_DIR`. These are unpacked raw model history files on tiles.

### Launching an interactive run

- As with compilation, users can launch an interactive batch session, and then run the model interactively. Simply start the batch session with the appropriate resources, copy in the inputs and executables, start the container, and run the model. This might look something like:

```
cd ~/scratch

# Launch interactive session
salloc --time=1:0:0 --constraint=cascade --nodes=1 --ntasks-per-node=48 --mem-per-cpu=1000

# Move to SLURM_TMPDIR to run, since ~
cd $SLURM_TMPDIR

# Copy in the input files and executables
cp ~/project/$USER/canesm_input_data/piControl/* .
cp ~/project/$USER/canesm_exes/* .                  # must have AGCM, CPL and nemo.exe

# Create a conf. file that allows us to run the model in multiple program multiple data mode. 
# SLURM does not support this very well. Note the specification of CPUs for each exe. The total
# number cannot exceed the resource request.

CONTAINER_IMAGE=~/project/$USER/singularity_images/canesm-docker_latest.sif

echo -e "0-15 singularity exec ${CONTAINER_IMAGE} ./AGCM \n 16 singularity exec ${CONTAINER_IMAGE} ./CPL \n 17-41 singularity exec $CONTAINER_IMAGE} ./nemo.exe" > run.conf

# Launch the run
export OMP_NUM_THREADS=1
time srun -n 42 -l --multi-prog run.conf 
```

## Post-processing model data

- Creating usable output from the raw model history files requires several steps (repacking, joining tiles, computing diagnostics, converting to timeseries, and finally conversion to CMOR compliant NetCDF). Within ECCC, this is achieved by a diagnostic string, which comprises many jobs, and relies on several pieces of software, including the CanAM diagnostics package (CanDIAG). At this time, this processing pipeline has not been ported to Cedar, but work is ongoing.

