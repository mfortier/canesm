Modifying CanESM
================

This content is adapted from the `Developing_readme.md` in the CanESM super repository.

Introduction
++++++++++++

This document describes the basic steps required to modify the CanESM repository.
It should be noted that, as described in the :ref:`CanESM version control
guidance <Forking Workflow>`, the ``CanESM`` repository is built
as a "super-repo", where various components are tracked as "sub-repositories"
or "submodules". To help interact with these submodules, special ``git s*`` scripts
have been added to the super-repo, under ``CCCma_tools/tools``, and are shown below
in various command examples - the suite of these tools, known as "*s-scripts*" are 
described :ref:`here <The S-Scripts>`.

Making Changes
++++++++++++++

1. **Create an issue**

    Before beginning work you should create an issue describing your intentions
    in the CanESM issue tracker. This allows us to keep track of what was done
    when and why. When setting up the issue, you should have a clear starting
    point (i.e. super-repo SHA1).

2. **Clone the CanESM super-repo, to create an on-disk repo**

    .. code-block:: shell

        git clone --recursive git@GIT_ADDRESS.git

    where ``git@GIT_ADDRESS.git`` is to be replaced with the desired remote
    address - for example:
    
    - ``git@gitlab.science.gc.ca:CanESM/CanESM5.git``: central, internal ECCC repository
    - ``git@gitlab.com:cccma/canesm.git``: central, public repository
    - ``git@gitlab.science.gc.ca:user123/CanESM5.git``: ``user123``'s fork, internal ECCC repository
    - ``git@gitlab.com:user123/canesm.git``: ``user123``'s fork, public repository

    |
    .. note:: As described in the :ref:`version control guidance <Forking Workflow>`, the CanESM
              development workflow operates as a "forked workflow", meaning that developers
              are **not** able to push to the central repositories. Therefore, it is 
              recommended that developers clone directly from their user fork.

3. **Checkout named branches**

    To begin development, users should create and checkout a new branch in the
    super-repo, **and** in each submodule. Instead of navigating into each repository, 
    developers can simply use ``git sbranch`` or ``git scheckout`` at the super-repo
    level. For example, if the branch does not yet exist, then:
    
    .. code-block:: shell
        
        # at the super repo level
        git scheckout -b BRANCH-NAME 

    where BRANCH-NAME is the name of your custom branch. If the BRANCH-NAME already exists, then do
    
    .. code-block:: shell

        git scheckout BRANCH-NAME
    
    You can now go ahead and modify any code you like (in any submodule).

    .. note::
        
        For older versions of ``git scheckout``, the ``-b`` option might not work. If this
        happens, run:

        .. code-block:: shell

            git sbranch BRANCH-NAME     # create the branch without checking it out
            git scheckout BRANCH-NAME  # check it out

    .. warning::

        It is important that developers are aware of the branches/commits that 
        are checked-out in the submodules before proceding with development, 
        **particularly just after cloning the code** (whether through ``git clone`` 
        or any of the ``setup-*`` scripts). Right after the clone, the 
        **submodules are NOT checked out to a specific branch** and are instead
        in a "Detached Head State", which means that instead of a branch the 
        sub-repo is checked out to the *specific commit* that are registered to 
        the super-repo branch. To alleviate this, developers **must** checkout
        the desired branch (across all repos) before proceding with development. 
        
        To make it easier to check branches across all repos, developers can use

        .. code-block:: shell

            git sbranch 

        which affords output like:
        
        .. code-block:: text 

           * (HEAD detached at 6b95543)

        if any repo is in a detached head state.
 

4. **Add and commit the changes**

    - **using s-scripts:**

        To seamlessly stage changes and new files across *all* the submodules and the
        super-repo automatically, developers can use the *s-script* version of ``git add``.
    
        .. code-block:: shell

            git sadd
        
        which will navigate into each submodule and stage ``any`` new files or changes. 
        Once this is done, developers can then commit all the changes by executing:

        .. code-block:: shell

            git scommit 
            
        which will then require you to leave custom commit messages for each repo modified.

    - **using raw git commands:**
    
        As mentioned above, using the s-scripts will stage **all** new files
        and changes. Some developers may wish to avoid this (i.e. if they don't
        want to commit everything at once, or if they have temporary files
        lying around). If this is the case, developers should:

        1. Navigate to the desired submodule.
        2. ``git add``/``git commit`` the desired changes.
        3. Navigate back to the super-repo.
        4. ``git add``/``git commit`` the new sub-module commit.

5. **Push changes back to gitlab**

    The easiest way to push changes back to the desired remote is to use the
    s-script version of ``git push``. Specifically:

    .. code-block:: shell
    
        # at the super-repo level
        git spush REMOTE-NAME BRANCH-NAME

    where ``REMOTE-NAME`` is the name of the remote you wish to push too. In
    most cases ``REMOTE-NAME``, will be ``origin``, which by default points to
    the remote from which you cloned the respository in the first place - more
    on this can be found :ref:`here <The S-Scripts>`.

6. **Launch a test run from gitlab**
    
    Once the changes are committed and pushed, developers should then launch 
    a test run from scratch, using the modified code. To do this, see the running guides
    for:

        - :ref:`Running CanESM on ECCC HPC systems`
        - :ref:`Running CanESM on Compute Canada / Cedar (Singularity)` - *in beta*
        - :ref:`Running CanESM on Google Cloud Platform (Docker)` - *in beta*
       
7. **Merge in the main branch and handle any conflicts.**

    Once you are satisfied with your changes, **you are responsible for
    confirming that your changes do not conflict with other work done to**
    **the main branch**, ``develop_canesm``. To do this, you must merge in
    ``develop_canesm`` (from the central repository) into your development
    branch, handle any conflicts, and commit/push the updated branch. If any
    *new* changes were brought in via this merge, you should then launch
    another test to make sure the code is still operational. 

    The process for handling mergers, and accompanying documentation, can be
    found :ref:`here <CanESM Version Control System>`

8. **Create a merge request upon completion** 

    Once you have determined that your updates work with the ``HEAD`` of
    ``develop_canesm`` merged into your branch, you should then make a merge
    request (MR) on gitlab, between your branch and ``develop_canesm``,
    for the super-repo **and** every affected submodule.
    
    As part of this MR, **you must select at least one** person to review your changes, and:

      1. explain what this MR is designed to fix/update
      2. provide the reviewer with a description of how you tested your changes

        .. note:: 

           As part of this merge request, please provide a link to the submodule 
           MRs as part of the *main* MR at the super repo level. 

    Upon approval of your changes, and a passing of a Continuous
    Integration (CI) pipeline, your code may be officially
    accepted and merged into ``develop_canesm``.

Deleting old branches
^^^^^^^^^^^^^^^^^^^^^

It is recommended that users clean old branches from their repositories 
once the changes are merged into the main repo's main branch. To do this
across submodules, both locally and on the remote pointed to via ``origin``

.. code-block:: shell

    git sdel BRANCH-NAME

.. note:: 

    ``git sdel`` currently only supports interacting with ``origin`` and automatically
    attempts to delete both local branchs **and** those on ``origin``. If users would like
    different functionality, they are encouraged to consult with other online documentation.
