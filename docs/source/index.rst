
CanESM user guide
=================

The Canadian Earth System Model is comprehensive representation of the climate
system used to simulate past climate change, to make projections of future
climate change, and to make initialized seasonal and decadal predictions.

CanESM is developed at the `Canadian Centre for Climate Modelling and Analysis <cccma.ec.gc.ca>`_,
Climate Research Division, Environment and Climate Change Canada.

This guide provides an overview of the modelling system and
practical usage guidance on configuring and running climate simulations.
The scientific documentation of the model is provided separately, and referenced
in the overview section.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   quickstart/quickstarts	     
   overview
   code
   contributing
   advanced
	     
.. WARNING::
   This is a test of a documentation system. It is not official documentation.
   There is currently no support available whatsoever for using CanESM, and we
   do not endorse or certify any use external to ECCC at this time.


