
Overview of CanESM
==================

.. contents:: :local:

History and application
+++++++++++++++++++++++

The Canadian Earth System Model is a global model developed to simulate historical
climate change and variability, to make centennial-scale projections of future climate,
and to produce initialized seasonal and decadal predictions. CanESM has a pedigree
extending back over 40 years, having built on many preceeding model versions. CanESM,
its predecessors and components have been widely applied for various phases of 
the Coupled Model Intercomparison Project (`CMIP <https://www.wcrp-climate.org/wgcm-cmip>`_),
the Chemistry Climate Modelling Initiative (`CCMI <https://www.sparc-climate.org/activities/ccm-initiative/>`_),
for operational seasonal prediction at Environment Canada, and many other scientific applications.

Well over 200,000 years of climate simulation have been conducted with CanESM, and the results have appeared
in hundreds of peer reviewed journal publications, Intergovernmental Panel on Climate
Change (IPCC) Assessment Reports, World Meterological Association Ozone Assessements, and Artic Monitoring
and Assessment Program Reports, amongst others.

Selected CanESM output for official CanESM simulations is publicly available on the
`ESGF <https://esgf-node.llnl.gov/projects/esgf-llnl/>`_, and on the
`CCCma website <https://esgf-node.llnl.gov/projects/esgf-llnl/>`_.

Model description
+++++++++++++++++

A complete description of CanESM5 and its components is given in
`Swart et al. (2019) <https://gmd.copernicus.org/articles/12/4823/2019/>`_,
and references therein. A brief summary is provided here:

=========  ===============================================================================================
Component  Description
=========  ===============================================================================================
CanAM      Canadian Atmosphere model, a global, 3D general circulation model
CLASSIC    The Canadian Land Surface Scheme and Canadian Terrerestrial Ecosystem Model (embedded in CanAM)
CanNEMO    NEMO ocean model modified for CanESM, including ocean biogeochemistry
CanCPL     The Canadian Coupler, which connects CanAM and CanNEMO
=========  ===============================================================================================

Scientific documentation
++++++++++++++++++++++++

The peer reviewed literature provides the definitive source of documentation
for the scientific aspects of CanESM. Some scientific documentation also
appears inline in the model routines. Key publications and
scientific user guides are referenced below.

Overview of basic operations 
++++++++++++++++++++++++++++

At a high level, CanESM must be configured, compiled and run, and then outputs must be
post-processed. Each step is treated briefly below, and more completely in the remainder
of the guide. 

Configuration
-------------
Configuration is achieved via cpp directives at compile time, and via various namelists at runtime.
A set of configuration utility scripting is normally used to handle creating consistent model setups, including
specifying cpp and namelist settings, configuring consistent component timing, specifying the correct
inputs and outputs, and controlling the runtime and post-processing sequencing.

Compilation
-----------

The code for CanAM (including CLASS/CTEM), CanCPL and CanNEMO are each compiled
into a separate executable. Hence any complete CanESM run uses three executables.
Ocean only runs use only the NEMO executable, while atmopshere only runs (AMIP)
rely on CanAM and CanCPL. Compilation is nominally achieved via standard Makefiles,
with some additional utility scripting.

Runtime
-------

The model requires various inputs at runtime, including configuration namelists, and
various forcing or data files. Each component requires unique inputs, which depend on
the experiment and configuration. Each model component also has its own restart files,
which are recorded at least once at the end of a block of simulation. The coupled
model is currently always started from an existing restart.

The three executables communicate over MPI during a simulation. Hence, runs are launched
in so called Multiple Program Multiple Data Mode. Each component
controls its own timing. Information is passed between CanAM and CanNEMO, via CanCPL,
at a specified coupling interval. During the course of a run, each component will
write out data into raw history files, as well as various log files.
CanAM input and output occurs in CCCma format, whereas CanNEMO and
CanCPL use NetCDF formatted files. Logs are all plain text format.

Long periods of simulation are generally run in chunks. The optimal chunk size
depends on the wallclock limits and stablity of the machine being used. A set of
sequencing infrastructure is typically used to orchestrate the running of many chunks to
form a complete simulation, including various file handling, and potentially the
launching of diagnostics which post-process the model output. 

Post-processing
---------------

Various post-processing of raw model history files is required, nominally joining tiles
into global files; producing appropriate time averages, spatial transforms, and derived
quantities. The final stage of post-processing is the creation of CMIP-compliant NetCDF
data. An extensive set of diagnostic tools exists to achieve this post processing, whose
complexity and expense rivals that of the model itself.


CanESM key references
+++++++++++++++++++++

**Coupled Model and CanESM5 updates**

* `Swart et al. (2019) <https://gmd.copernicus.org/articles/12/4823/2019/>`_

**CanAM**

* Cole et al. (in prep.)

* `von Salzen et al. (2013) <https://doi.org/10.1080/07055900.2012.755610>`_

* CanAM scientific user guide (not currently redered - see documentation inline with code). 

**CLASS / CTEM**

* `CLASSIC documentation (may be more recent than used in CanESM) <https://cccma.gitlab.io/classic/index.html>`_

* `Arora and Scinocca (2016) <https://doi.org/10.5194/gmd-9-2357-2016>`_
  
**CanNEMO, CMOC and CanOE**

* `NEMO user manuals <https://www.nemo-ocean.eu/bibliography/how-to-cite/>`_

* `Christian et al. (2010) <https://doi.org/10.1029/2008JG000920>`_  
  
* `Zahariev et al. (2008) <https://doi.org/10.1016/j.pocean.2008.01.007>`_ 
  
