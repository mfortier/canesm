#!/bin/bash
set -e

. REPLACEMENT_PATH_TO_SOURCE

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# config file
canesm_cfg_file=canesm.cfg
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

# set a bail function
bail(){
    echo "MAKE_JOB: $*"
    exit 1
}

# Import any other bash functions needed
. $CCRNSRC/CanESM/CCCma_tools/tools/mod_nl.sh

# Source the config file
. ${canesm_cfg_file}

# Create a scratch directory for this file 
make_job_temp_dir=make_job_temp_dir
mkdir -p $make_job_temp_dir

# if netcdf conversion is on - check/update experiment table entry
if (( with_netcdf_conv == 1 )) ; then
  pop_nc_tables config=${canesm_cfg_file}
fi

# Run code checking and update logs
[ $production -eq 1 ] && flgs="" || flgs="-d"   # use 'development' mode if production is off
strict_check $flgs $runid create-job-string canesm.cfg make_job_${runid}
[ $? -ne 0 ] && exit 1

# derive year_start and month_start
start_list=(${start_time//:/ })
start_year=${start_list[0]:-1}
start_month=${start_list[1]:-1}

# derive year_stop and month_stop
stop_list=(${stop_time//:/ })
stop_year=${stop_list[0]:-1}
stop_month=${stop_list[1]:-12}

# derive year_rtdiag_start and month_rtdiag_start
start_rtdiag_list=(${start_rtdiag//:/ })
year_rtdiag_start=${start_rtdiag_list[0]:-1}
month_rtdiag_start=${start_rtdiag_list[1]:-1}

# Check consistency of $year_rtdiag_start vs $start_time
if [ $year_rtdiag_start -ne $start_year -o $month_rtdiag_start -ne $start_month ] ; then
  echo "WARNING: year_rtdiag_start=$year_rtdiag_start != start_year=$start_year or/and month_rtdiag_start=$month_rtdiag_start != start_month=$start_month !!!"
  read -p "Do you want to continue? y/[n] " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
    echo "Abort."
    exit 1
  fi
fi

printf -v date1 "%04dm%02d" $start_year $start_month
printf -v date2 "%04dm%02d" $stop_year $stop_month
if [[ ${#start_list[@]} -gt 2 ]]; then
  [ -n "${start_list[2]}" ] && printf -v d1 "d%02d" ${start_list[2]} && date1+="$d1"
fi
if [[ ${#stop_list[@]} -gt 2 ]]; then
  [ -n  "${stop_list[2]}" ] && printf -v d2 "d%02d" ${stop_list[2]}  && date2+="$d2"
fi
range="${date1}_${date2}"
fout="${runid}_${range}_${TRUEHOST}_job"

# Create the job
stamp=`date "+%j%H%M%S"$$`
jobdefs=jobdefs_modljob_$stamp
modljob_sfx=modljob_${TRUEHOST}_$stamp

# Create jobdefs file with all variable definitions
cat ${canesm_cfg_file} | bash -x - 2>&1 | grep '^+ ' | sed 's/+ //' | grep '[^ ]=[^ ]' > $jobdefs
# reverse the order of variables as only the first value is used by cccjob when a variable is defined multiple times.
tac $jobdefs > $jobdefs.rev
mv $jobdefs.rev $jobdefs

cccjob --out=$fout --job="${model_basefile}:m" --start=$start_time --stop=$stop_time crawork=${runid}_${modljob_sfx} $jobdefs
rm -f $jobdefs

#------------------------------------------------------
# Extract, modify, and save namelists used in this run
#------------------------------------------------------
pushd $make_job_temp_dir

#-- AGCM namelist
target_agcm_nl=${WRK_DIR}/modl.dat_${runid}
target_agcm_nl_basename=$(basename ${target_agcm_nl})

# determine the number of radiative forcing calls
# 	Note: this is currently duplicated in the basefile
case $RF_SCENARIO in
   	SRESA1B|SRESA2|SRESB1) NRFP=1 ;;
     				2XCO2) NRFP=1 ;;
   				  2X4XCO2) NRFP=2 ;;
			   4XCO2_GHAN) NRFP=2 ;;
    		  	   CFMIPA) NRFP=3 ;;
    			   AERORF) NRFP=6 ;;
     			    PAMRF) NRFP=6 ;;
         			    *) NRFP=1 ;;
esac

# if iyear is populated make name consistent with namelist param
[[ -n $iyear ]] && IYEAR=$iyear

# make the modifications
cp $target_agcm_nl $target_agcm_nl_basename
mod_nl $target_agcm_nl_basename ioztyp     \
                                ioxtyp     \
                                IYEAR      \
                                NRFP       \
                                nnode_a    \
                                lndcvrmod

# save modified namelist and overwrite the original (unmod'd version)
cp -f $target_agcm_nl_basename $target_agcm_nl
save $target_agcm_nl_basename $target_agcm_nl_basename
unset NRFP
unset IYEAR

#-- CanCPL namelist
# determine the ocean dimensions
case $nemo_config in
	  *_ORCA1_*) nlon_o=360  ; nlat_o=292  ;;
	*_ORCA025_*) nlon_o=1440 ; nlat_o=1021 ;;
esac

# make the modifications
env_runid=$runid # Make a copy to match variable in CanCPL namelist
cp $CCRNSRC/CanESM/CanCPL/lib/nl_coupler_par ./
mod_nl nl_coupler_par windstress_remap              \
                      landfrac_bug                  \
                      cpl_rs_abort_if_missing_field \
                      bulk_in_cpl                   \
                      _char_env_runid               \
                      couple_serial                 \
                      nlon_o                        \
                      nlat_o
save nl_coupler_par nl_coupler_par
unset env_runid
unset nlon_o
unset nlat_o

#TODO: add namelist configuration from other tools here
# (i.e. inline nl and phys parm nl in save/tapeload restarts)

popd

#------------------------------------------------------------------
# print out some parameters affecting dumping and deletion of files
#------------------------------------------------------------------
echo ""
if [ $with_dump_rs -eq 1 ] ; then
  if [ "$canesm_dump_histRS_short_term" = "on" ] ; then
    echo "Restart     files (mc_*) will be archived to short term."
  else
    echo "Restart     files (mc_*) will be archived to long term."
  fi
else
  echo "Restart     files (mc_*) won't be archived."
fi
if [ $with_dump_hist -eq 1 ] ; then
  if [ "$canesm_dump_hist_short_term" = "on" ] ; then
    echo "Model hist. files (mc_*) will be archived to short term."
  else
    echo "Model hist. files (mc_*) will be archived to long term."
  fi
else
  echo "Model hist. files (mc_*) won't be archived."
fi
if [ $with_dump_diag -eq 1 ] ; then
  if [ "$canesm_dump_diag_short_term" = "on" ] ; then
    echo "Diagnostic  files (dc_*) will be archived to short term."
  else
    echo "Diagnostic  files (dc_*) will be archived to long term."
  fi
else
  echo "Diagnostic  files (dc_*) won't be archived."
fi
if [ $with_dump_tser -eq 1 ] ; then
  if [ "$canesm_dump_tser_short_term" = "on" ] ; then
    echo "Time series files (sc_*) will be archived to short term."
  else
    echo "Time series files (sc_*) will be archived to long term."
  fi
else
  echo "Time series files (sc_*) won't be archived."
fi

echo ""
if [ $with_delrsFE -eq 1 ] ; then
  echo "Restart     files (mc_*) will be deleted from disk (except for last)."
else
  echo "Restart     files (mc_*) will be left on disk."
fi
if [ $with_delhistFE -eq 1 ] ; then
  echo "Model hist. files (mc_*) will be deleted from disk."
else
  echo "Model hist. files (mc_*) will be left on disk."
fi
if [ $with_deldiag -eq 1 ] ; then
  echo "Diagnostic  files (dc_*) will be deleted from disk."
else
  echo "Diagnostic  files (dc_*) will be left on disk."
fi
if [ $with_netcdf_conv -eq 1 ] ; then
  echo "Time series files (sc_*) will be converted to netcdf format."
else
  echo "Time series files (sc_*) won't be converted to netcdf format."
fi
if [ $with_del_tser -eq 1 ] ; then
  echo "Time series files (sc_*) will be deleted from disk."
else
  echo "Time series files (sc_*) will be left on disk."
fi

# Remove any imported functions
unset -f mod_nl
rm -rf $make_job_temp_dir
