!   ---- Various model cpp directives that control diagnostic output in
!        the AGCM.

!   ---- Set the saving of extra chemical diagnostic fields.
#define xtrachem

!   ---- Set the saving of extra convective diagnostic fields.
#define xtraconv

!   ---- Set the saving of extra cloud microphysics diagnostic fields.
#undef xtrals

!   ---- Set saving of radiative flux profiles (all and clear-sky, up and down)
!   ---- Note that it will also save flux profiles for diagnostics radiative
!   ---- forcing calculations as well if "radforce" defined.
#define rad_flux_profs

!   ---- Set the calculation and saving of radiative forcing fields
#undef radforce

!   ---- If radiative forcing being computed the flux profiles are needed.
#if defined radforce
#define rad_flux_profs
#endif

!   ---- Set the use of COSP
#define use_cosp

!   ---- Set the saving of results for aerosol optical calculations.
#define aodpth

!   ---- Set the saving of diagnostics for updated dust emission scheme.
#define xtradust

#if defined xtradust || defined aodpth
! ---- Include extra chemical diagnostic code in AGCM for consistency
#define xtrachem
#endif

!   ---- Extra diagnostics related to sulfates
#undef xtrasulf

!   ---- Diagnostics to save the "right-hand side" tendencies

! - Save the total tendency
#define tprhs
#define qprhs
#define uprhs
#define vprhs
#define xprhs

! - Save the component tendencies
#define tprhsc
#define qprhsc
#define uprhsc
#define vprhsc
#define xprhsc

! - Save the dynamic tendencies (t,q,u and v)
#undef dynrhs

!   ---- Saving of select arrays

#undef x01
#undef x02
#undef x03
#undef x04
#undef x05
#undef x06
#undef x07
#undef x08
#undef x09

!   ---- Set the use of high frequency output at fixed sites
#undef cf_sites

!   ---- If site data is being saved flux profiles and xtrals are needed.
#if defined cf_sites
#define rad_flux_profs
#define xtrals
#endif

!   ---- include AGCM mpi, coupler activation keys
#define with_MPI_
#define with_COUPLED_

!   ---- include mpi cpp key for the coupler
#define use_mpi
