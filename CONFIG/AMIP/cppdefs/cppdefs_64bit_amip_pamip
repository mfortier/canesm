!   ---- Various model run-time parameters converted from update
!        directives

!   ---- Set the use of solar variability.
#undef with_solvar

!   ---- Set the use of explosive volcanoes.
#define explvol

!   ---- Set the (possible) use of modeling inland lakes.
!   ---- These two choices are mutually exclusive and
!   ---- an abort condition in the model checks for this.
!   ---- In addition, neither should be set if not running
!   ---- with a lake tile and an abort condition in the
!   ---- model also checks for this.
#undef flake
#define cslm

!   ---- Set the use of McICA
#define use_mcica

!   ---- Set the choice for surface albedo calculation (NOTE:
!        mutually exclusive!).
#define salbtable
#undef salbform1

!   ---- Set the use of AMIP vs climatological boundary layer
!   ---- forcing (NB: non-coupled runs only!!!).
#undef myrssti

!   ---- Set the saving of fields with date/time stamps.
#undef isavdts

!   ---- Use new snow albedo paramterization (defined) or CLASS 3.6 parameterization (undef)
#define isnoalb

!   ---- Use PAM aerosol model.
#undef pla
#undef pam
#if defined pla
!   ---- Use aerosol and cloud droplet forcing data set.
#undef pfrc
!   ---- Switches for individual forcing agents.
#undef amradfrc
#undef ssradfrc
#undef dsradfrc
#undef bcradfrc
#undef ocradfrc
#undef cdncfrc
#undef bcicfrc
#undef bcdpfrc

!   ---- Set the saving of results for extra PLA output.
#undef xtraplafrc
#undef xtrapla1
#undef xtrapla2
#endif

#undef rcm
#undef mam
#undef mamchem

#undef timers
#define parallel_io

!   ---- Select one (only) of the following for model resolution.
#undef model192x96v5
#undef model192x96l29_3x3v2
#undef model256x192l29
#undef model256x192l33v1
#define model256x192l40v1
#undef model256x192l45v1

!   ---- Select operating mode(s) for the coupler
#define coupler_cgcm
#undef coupler_sa
#undef coupler_debug

!   ---- Flag for selecting use of the biogeochem option
!#undef biogeochem

!   ---- Flag for using the ctem code
#define agcm_ctem

!   ---- Flag for using river routing scheme in AGCM
#define agcm_river_routing

!   ---- Check to see if either of the above options are set to run in
!   ---- carbon cycle mode. If so, set the "carbon" flag

#undef carbon
#if defined biogeochem || defined coupler_ctem || defined agcm_ctem
!   ---- Include carbon model specific code in AGCM
#define carbon

!   ---- Flag for using cmoc cholorophyll for solar heating (defined)
!   ---- or not. If cmoc is not defined, then the observed chlorophyll
!   ---- will be used (read from file).
#define bio_cmoc_chl_solar_heating

!   ---- Flag for running free-co2 but specifying CO2 in lowest level (only)
!   ---- based on specified CO2 concentrations and a correction for the
!   ---- annual mean. NOTE! **NOT** to be run with "specified_co2"!!
!   ---- NOTE! For this option, or for "free_co2", one must "deke" the
!   ----       the conservation constants for CO2 if starting from a
!   ----       "specified_co2" restart!!!!!!!!!!!!!!!
#undef specified_anncycle_co2_lowlev

!   ---- Flag for using inverse calculation of emissions from concentrations
!   ---- NOTE: Only active when "carbon" switch is!!!
#define specified_co2

!   ---- Flag for using decoupled radiation (ie GHG constant) in full
!   ---- carbon cycle model
!   ---- NOTE: Only active when "carbon" switch is!!!
#undef decoupled_rad_co2
#endif

!   ---- Flag to set instantaneous doubling of CO2
#undef co2_x2

!   ---- Flag to set instantaneous quadrupiling of CO2
#undef co2_x4

!   ---- Flag to set instantaneous octupling of CO2
#undef co2_x8

!   ---- Flag to set 1% per year increase of CO2
#undef co2_1ppy

#undef histemi
#define emists
!   ---- transient aerosols switch.
!   ---- NOTE! There must be consistency with the above parmsub variable
!   ----       "em_aerosols" as noted in the comments in that section.
#undef transient_aerosol_emissions
#if defined emists || defined histemi
!   ---- Include transient_aerosol_emissions model specific code in AGCM
#define transient_aerosol_emissions
#endif

#define histoz
!   ---- transient ozone switch.
!   ---- NOTE! There must be consistency with the above parmsub variable
!   ----       "conc_ozone" as noted in the comments in that section.
#undef transient_ozone_concentrations
#if defined histoz
!   ---- Include transient_ozone_concentrations model specific code in AGCM
#define transient_ozone_concentrations
#endif

!   ---- Select whether specified aerosol loading or not ***through coupler***
#undef coupler_aerosols

!   ---- Select greenhouse gas code for coupler
#undef coupler_ghg

#define cfc11_effective
#ifdef cfc11_effective
!   ---- Use "effective" CFC11 concentrations that are representative of
!   ---- a combination of CFC11 and a number of other GHGs whose effects
!   ---- are not explicit in the model
#endif

!   ---- Read specified boundary conditions from a file in the coupler
!   ---- The 3D ocean code will not run (=> no gz,cm,os files are created)
!   ---- The value of this macro determines the type of boundary conditions
!   ----   0 - Do not read boundary conditions from a file
!   ----   1 - climatological boundary conditions (12 monthly averages)
!   ----   2 - climatological boundary conditions (365 daily averages)
!   ----   3 - multi-year boundary conditions (monthly averages)
!   ----   4 - multi-year boundary conditions (daily averages)
#define coupler_specified_bc 3

!   ---- If defined, coupler_specified_bc_file must be the name of the file
!   ---- containing specified boundary conditions read through the coupler.
!   ---- If coupler_specified_bc_file is not defined and monthly averaged
!   ---- climatological boundary conditions are used (coupler_specified_bc = 1)
!   ---- then the "AN" file will be used, otherwise a runtime error will occur.
!   ---- Choose one of the following if activating:
#define coupler_specified_bc_file coupler_specified_bc_file

!   ---- If defined, ctem_write_spec_file forms part of a file name for a file
!   ---- containing the daily CTEM data that is passed back to the atm at each
!   ---- coupling interval. This file name will be the value of ctem_write_spec_file
!   ---- appended with either "_YYYY_mMM" (for monthly) or "_YYYY" (for yearly)
!   ---- depending on the value of ctem_write_spec_type
!#undef ctem_write_spec_file mc_eau_ctem_daily_data

!   ---- ctem_write_spec_type determines the type of file created when
!   ---- ctem_write_spec_file is defined
!   ---- The default is 1 if ctem_write_spec_type is not defined
!   ----    ctem_write_spec_type = 1  means write yearly files of daily data
!   ----    ctem_write_spec_type = 2  means write monthly files of daily data
!#undef ctem_write_spec_type 1

!   ---- If defined, ctem_read_spec_file forms part or all of a file name for a file
!   ---- containing daily CTEM data that is read read from a file in the coupler.
!   ---- This daily CTEM data is passed to the atm and will replace similar fields
!   ---- that would normally be created in the coupler then passed to the atm.
!   ---- The name of the file that is read will be the value of ctem_read_spec_file
!   ---- when ctem_read_spec_type = 1
!   ---- The name of the file that is read will be the value of ctem_read_spec_file
!   ---- appended with "_YYYY_mMM" when ctem_read_spec_type = 2 or the value
!   ---- of ctem_read_spec_file appended with "_YYYY" if ctem_read_spec_type = 3
!#undef ctem_read_spec_file mc_eau_ctem_daily_clim_1971_2030

!   ---- ctem_read_spec_type determines the type of file read when
!   ---- ctem_read_spec_file is defined
!   ---- The default is 1 if ctem_read_spec_type is not defined
!   ----    ctem_read_spec_type = 1  means read a yearly climatology of daily data
!   ----    ctem_read_spec_type = 2  means read daily data ...one month per file
!   ----    ctem_read_spec_type = 3  means read daily data ...one year per file
!#undef ctem_read_spec_type 1

!   --- If use_NEMO is defined then the ocean model is NEMO
#undef use_NEMO

!   --- If use_canam_32bit is defined then use CanAM in 32-bit mode
#undef use_canam_32bit

!   ---- Set the use of relaxation/nudging
#undef relax

!   ---- include AGCM mpi, coupler activation keys
#define with_MPI_
#define with_COUPLED_

!   ---- include mpi cpp key for the coupler
#define use_mpi
