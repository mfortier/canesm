#! /bin/sh
set -e
# Compilation script for CanESM - OMIP configuration

Usage='Usage:

  compile-canesm.sh [-f] [-h]

  -f force a clean compilation
  -h display this usage message!

  Compile CanNEMO for CanESM OMIP configuration
'
#=======================================
# Make sure the environment has been set
#=======================================
if [[ -z "$CCRNSRC" ]]; then
    echo "The run code directory, CCRNSRC has not been set! Source env_setup_file and try again!"
    exit 1
fi

#======================
# Set Default variables
#======================
canesm_cfg_file=canesm.cfg
exec_strg_dir=${CCRNSRC}/executables

#==============
# Parse CL Args
#==============
clean_compile=0
while [[ "$#" -gt 0 ]] ; do
    case $1 in
        -f) clean_compile=1 ;;
         *) echo "Ignoring $1! The compilation script does not need any arguments!" ;;
    esac
    shift
done

#======================
# Setup Run Environment
#======================
# Source the config file
. ${canesm_cfg_file}

# define target names
nemo_target=${exec_strg_dir}/${nemo_exec}

#==================================
# Run code checking and update logs
#==================================
[[ $production -eq 1 ]] && flgs="" || flgs="-d"   # use 'development' mode if production is off
strict_check $flgs $runid compile canesm.cfg compile_${runid}
[[ $? -ne 0 ]] && exit 1

#================
# Compile CanNEMO
#================
echo "Compiling CanNEMO..."

# build array of arguments for build-nemo 
#   -> array needed to handle space delimited list that may come from add_key/del_key
NEMO_repo_path=${CCRNSRC}/CanESM/CanNEMO
nemo_opts=( srcpath=${NEMO_repo_path} exec=${nemo_exec} cfg=${nemo_config} arch=${nemo_arch} )
[[ -n "$nemo_add_cpp_keys" ]] && nemo_opts+=( add_key="${nemo_add_cpp_keys}" )
[[ -n "$nemo_del_cpp_keys" ]] && nemo_opts+=( del_key="${nemo_del_cpp_keys}" )
(( clean_compile == 1 ))      && nemo_opts=( "-f" "${nemo_opts[@]}")

# compile and save nemo executable
# Note: we may prefer to save the executable in the same way as the other executables
echo 'build-nemo -p $CCRNSRC/executables -j 12 '"${nemo_opts[@]}" > ${WRK_DIR}/.compile-canesm-cannemo-$$.log
build-nemo -p $exec_strg_dir -j 12 "${nemo_opts[@]}" >> ${WRK_DIR}/.compile-canesm-cannemo-$$.log 2>&1

echo "DONE!"
echo "Check for compilation errors in .compile-canesm-* !"

#========================================================================
# Bundle Execs into directory on $RUNPATH and save for archiving purposes
#========================================================================
# NOTE: this is ONLY done to maintain convention with the restart archiving system
#   - during the run, executables are always retrieved from $exec_strg_dir - NOT the saved directory on runpath

# check that the target exists, else skip bundling
#-------------------------------------------------
if ! [[ -f $nemo_target ]]; then
    echo "$nemo_target doesn't exist or points to a broken link! Check the hidden compilation logs (ls -a)!"
    echo "It is likely that the compilation failed for $target!"
    exit 1
fi

# derive date string and set file prefix
#---------------------------------------
start_date=(${start_time//:/ }) # replace : with " " and turn into array
start_year=${start_date[0]}     # first element will always be the year
start_month=${start_date[1]:-1} # IF a mnth was given use it, else default to 1

if (( start_month == 1 )); then
    # we start from a restart from start_year-1, at the end of the 12 month
    save_year=$(( start_year - 1 ))
    save_mnth=12
else
    # just use previous month
    save_year=$start_year
    save_mnth=$(echo $start_month | awk '{printf "%02d", $1-1}')
fi
stored_exe_dir=mc_${runid}_${save_year}_m${save_mnth}_exes

# create exe directory and save
#------------------------------
cd $exec_strg_dir
mkdir $stored_exe_dir
cp $nemo_target $stored_exe_dir
save $stored_exe_dir $stored_exe_dir
rm -rf $stored_exe_dir
cd -
