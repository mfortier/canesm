#! /bin/bash
set -e
# Compilation script for CanESM5

Usage='Usage:

  compile-canesm.sh [-d] [-a] [-c] [-n] [-f] [-h]

  -d compile CanDIAG
  -a compile CanAM
  -c compile CanCPL
  -n compile CanNEMO
  -f force a clean compilation
  -h display this usage message!

  If none of the flags are specified, compile all components reusing files that 
  may have already been generated.
'
#=======================================
# Make sure the environment has been set
#=======================================
if [[ -z "$CCRNSRC" ]]; then
    echo "The run code directory, CCRNSRC has not been set! Source env_setup_file and try again!"
    exit 1
fi

#======================
# Set Default variables
#======================
canesm_cfg_file=canesm.cfg
cppdef_config_file=$(pwd)/cppdef_config.h
cppdef_sizes_file=$(pwd)/cppdef_sizes.h
exec_strg_dir=${CCRNSRC}/executables

#==============
# Parse CL Args
#==============
compile_diags=0
compile_atmos=0
compile_cpl=0
compile_nemo=0
clean_compile=0
while [[ "$#" -gt 0 ]] ; do
    case $1 in
        -d) compile_diags=1 ;;
        -a) compile_atmos=1 ;;
        -c) compile_cpl=1 ;;
        -n) compile_nemo=1 ;;
        -f) clean_compile=1 ;;
        -h) echo $Usage; exit ;;
         *) echo "Ignoring $1! The jobstring is no longer required!" ;;
    esac
    shift
done

# set all compile flags to 1 if they are all 0.
if (( compile_diags == 0 )) && (( compile_atmos == 0 )) && (( compile_cpl == 0 )) && (( compile_nemo == 0 )); then
    compile_diags=1
    compile_atmos=1
    compile_cpl=1
    compile_nemo=1
fi
echo compile_diags=$compile_diags
echo compile_atmos=$compile_atmos
echo compile_cpl=$compile_cpl
echo compile_nemo=$compile_nemo

#=======================
# Setup Run Environment
#=======================
# Source the config file
. ${canesm_cfg_file}

# define target names
agcm_target=${exec_strg_dir}/mc_${runid}_ab
cpl_target=${exec_strg_dir}/mc_${runid}_cpl.exe
nemo_target=${exec_strg_dir}/${nemo_exec}
diag_bin_dir=${exec_strg_dir}/bin_diag

# define the namelist that contains AGCM settings
#   - this is used to generate the cpp "sizes" file, but eventually the AGCM
#     should be updated to truely read these params in via a namelist read
agcm_namelist=modl.dat_${runid}

#==================================
# Run code checking and update logs
#==================================
[[ $production -eq 1 ]] && flgs="" || flgs="-d"   # use 'development' mode if production is off
strict_check $flgs $runid compile canesm.cfg compile_${runid}
[[ $? -ne 0 ]] && exit 1

#======================
# Generate cppdef files
#======================
# clean existing cppfiles if clean compile is requested
if (( clean_compile == 1 )); then
    [[ -f "$cppdef_sizes_file" ]] && rm $cppdef_sizes_file
    [[ -f "$cppdef_config_file" ]] && rm $cppdef_config_file
fi

# generate AGCM sizes file, which is (in addition to the AGCM) is used in the compilation of CanCPL and CanNEMO due to coupling interfaces
if [[ ! -f "$cppdef_sizes_file" ]]; then
    printf "\ngenerating $cppdef_sizes_file from $agcm_namelist\n"
    source activate py3_canesm-inf_beta
    set_sizes $agcm_namelist $cppdef_sizes_file
    conda deactivate
else
    printf "\n$cppdef_sizes_file already exists ...\n"
    printf "Using existing copy... delete it or compile with '-f' if you want a new one created from $agcm_namelist\n\n"
fi

# generate cpp file from canesm.cfg definitions
if [[ ! -f "$cppdef_config_file" ]]; then
    printf "\ngenerating $cppdef_config_file from $canesm_cfg_file\n"
    cat $cppdefs_file > $cppdef_config_file 
    cat $cppdefs_diag >> $cppdef_config_file
else
    printf "\n$cppdef_config_file already exists ...\n"
    printf "Using existing copy... delete it or compile with '-f' if you want a new one created from $canesm_cfg_file settings\n\n"
fi

#===========================
# Compile Desired Components
#===========================

#-------------------------------
# Compile Diagnostics / Programs
#-------------------------------
if (( compile_diags == 1 )) ; then

    # compare against reference library at CANDIAG_REF_PATH
    user_candiag_src=${CCRNSRC}/CanESM/CanDIAG
    user_tmp_md5sum_file=${WRK_DIR}/.user_candiag_md5sums
    ref_tmp_md5sum_file=${WRK_DIR}/.ref_candiag_md5sums
    candiag_updates=0

    # create checksum files
    get_candiag_checksum_file(){
        src_dir=$1
        md5sum_file=$2
        invoke_dir=$(pwd)
        cd $src_dir
        find lssub lspgm -type f -exec md5sum {} + | sort -k 2 > $md5sum_file
        md5sum diag_sizes.f90 >> $md5sum_file
        cd $invoke_dir
    }
    get_candiag_checksum_file $user_candiag_src $user_tmp_md5sum_file
    get_candiag_checksum_file $CANDIAG_REF_PATH $ref_tmp_md5sum_file

    # check for differences
    diff -u $user_tmp_md5sum_file $ref_tmp_md5sum_file > /dev/null 2>&1 || candiag_updates=1
    rm -f $user_tmp_md5sum_file
    rm -f $ref_tmp_md5sum_file

    # if there are differences, recompile, else use precompiled version
    CanDIAG_build_dir=${CCRNSRC}/CanESM/CanDIAG/build
    cd $CanDIAG_build_dir
    if (( candiag_updates == 1 )) || (( clean_compile == 1 )) ; then
        echo "Compiling CanDIAG..."

        # check if the reference library was previously linked in
        if [[ -L "bin" ]]; then
            make clean
        fi

        # clean if user requested it
        if (( clean_compile == 1 )); then 
            make clean
        fi

        source comp_env
        make -j 4 > ${WRK_DIR}/.compile-canesm-candiag-$$.log 2>&1
        echo "Compiling CanDIAG - DONE!"
    else
        echo "Using CanDIAG Reference Library..."
        make clean # clean existing dirs/links
        ln -s ${CANDIAG_REF_PATH}/build/bin bin
        ln -s ${CANDIAG_REF_PATH}/build/lib64 lib64
        ln -s ${CANDIAG_REF_PATH}/build/lib32 lib32
    fi
    cd $WRK_DIR

    # create merged diagnostic script
    make_merged_diag_script
fi

#---------------
# Compile CanAM 
#---------------
if (( compile_atmos == 1 )) ; then
    echo "Compiling CanAM..."
    AGCM_build_dir=${CCRNSRC}/CanESM/CanAM/build

    if (( clean_compile == 1 )); then
        ( cd $AGCM_build_dir; make clean )
    fi

    # setup build environment
    # compile and place into background
    #   - save happens below
    (   cd $AGCM_build_dir;                                   \
        source comp_env;                                      \
        make agcm32bit=$agcm32bit                             \
             CPP_CONFIG_FILE=$cppdef_config_file              \
             CPP_SIZES_FILE=$cppdef_sizes_file                \
             > ${WRK_DIR}/.compile-canesm-canam-$$.log 2>&1   \
    ) &
fi

#----------------
# Compile CanCPL
#----------------
if (( compile_cpl == 1 )) ; then
    echo "Compiling CanCPL..."
    CPL_build_dir=${CCRNSRC}/CanESM/CanCPL/build
    CPL_cppdef_config_link=${CPL_build_dir}/$(basename $cppdef_config_file)
    CPL_cppdef_sizes_link=${CPL_build_dir}/$(basename $cppdef_sizes_file)

    if (( clean_compile == 1 )); then
        # clean files/links in build dirs such that newly generated ones will be used
        [[ -f "$CPL_cppdef_sizes_link" ]]   && rm $CPL_cppdef_sizes_link
        [[ -f "$CPL_cppdef_config_link" ]]  && rm $CPL_cppdef_config_link
    fi

    # handle cpp sizes file
    if [[ ! -f "$CPL_cppdef_sizes_link" ]]; then
        # link in file
        ln -s $cppdef_sizes_file $CPL_cppdef_sizes_link
    fi

    # handle cpp config file
    if [[ ! -f "$CPL_cppdef_config_link" ]]; then
        # link in file
        ln -s $cppdef_config_file $CPL_cppdef_config_link
    fi

    # setup build environment
    cd $CPL_build_dir
    source comp_env

    # compile and place into background
    #   - save happens below
    (( clean_compile == 1 )) && make clean
    make > ${WRK_DIR}/.compile-canesm-cancpl-$$.log 2>&1 &
    cd $WRK_DIR
fi

#-----------------
# Compile CanNEMO
#-----------------
if (( compile_nemo == 1 )) ; then
    echo "Compiling CanNEMO..."
    # build array of arguments for build-nemo
    #   -> array needed to handle space delimited list that may come from add_key/del_key
    NEMO_repo_path=${CCRNSRC}/CanESM/CanNEMO
    nemo_opts=( srcpath=${NEMO_repo_path} exec=${nemo_exec} cfg=${nemo_config} arch=${nemo_arch} )
    [[ -n "$nemo_add_cpp_keys" ]] && nemo_opts+=( add_key="${nemo_add_cpp_keys}" )
    [[ -n "$nemo_del_cpp_keys" ]] && nemo_opts+=( del_key="${nemo_del_cpp_keys}" )
    (( clean_compile == 1 ))      && nemo_opts=( "-f" "${nemo_opts[@]}")

    # compile and save nemo executable
    # Note: we may prefer to save the executable in the same way as the other executables
    echo 'build-nemo -p $CCRNSRC/executables -j 12 '"${nemo_opts[@]}" > ${WRK_DIR}/.compile-canesm-cannemo-$$.log
    build-nemo -p $exec_strg_dir -j 12 "${nemo_opts[@]}" >> ${WRK_DIR}/.compile-canesm-cannemo-$$.log 2>&1 &
fi

wait
echo "Compiling ALL - DONE!"
echo "Check for compilation errors in .compile-canesm-* !"

#================================================
# Store Executables (ocean done within build-nemo)
#================================================
# create symlinks to build directories and store required executables
if (( compile_atmos == 1 )) ; then
    rm -f build_agcm    ; ln -s $AGCM_build_dir build_agcm
    rm -f $agcm_target  ; ln -s ${AGCM_build_dir}/bin/AGCM $agcm_target
fi
if (( compile_cpl == 1 )) ; then
    rm -f build_coupler ; ln -s $CPL_build_dir build_coupler
    rm -f $cpl_target   ; ln -s ${CPL_build_dir}/CPL $cpl_target
fi
if (( compile_diags == 1 )) ; then
    rm -f build_diag    ; ln -s $CanDIAG_build_dir build_diag
    rm -f $diag_bin_dir ; ln -s ${CanDIAG_build_dir}/bin $diag_bin_dir
fi

#========================================================================
# Bundle Execs into directory on $RUNPATH and save for archiving purposes
#========================================================================
# NOTE: this is only done to maintain convention with the restart archiving system
#   - during the run, executables are always retrieved from $exec_strg_dir - NOT the saved directory on runpath
if (( compile_atmos == 1 )) || (( compile_cpl == 1 )) || (( compile_nemo == 1 )); then

    # check that the target exists, else skip bundling
    #-------------------------------------------------
    targets="$agcm_target $cpl_target $nemo_target"
    for target in $targets; do
        if ! [[ -f $target ]]; then
            echo "$target doesn't exist or points to a broken link! Check the hidden compilation logs (ls -a)!"
            echo "It is likely that the compilation failed for $target!"
            exit
        fi
    done

	# derive date string and set file prefix
    #---------------------------------------
    start_date=(${start_time//:/ }) # replace : with " " and turn into array
    start_year=${start_date[0]}     # first element will always be the year
    start_month=${start_date[1]:-1} # IF a mnth was given use it, else default to 1

    if (( start_month == 1 )); then
        # we start from a restart from start_year-1, at the end of the 12 month
        save_year=$(echo $start_year | awk '{printf "%04d", $1-1}')
        save_mnth=12
    else
        # just use previous month
        save_year=$(echo $start_year | awk '{printf "%04d", $1}')
        save_mnth=$(echo $start_month | awk '{printf "%02d", $1-1}')
    fi
    stored_exe_dir=mc_${runid}_${save_year}_m${save_mnth}_exes

    # create exe directory and save
    #------------------------------
    cd $exec_strg_dir
    mkdir $stored_exe_dir
    cp $nemo_target $stored_exe_dir
    cp $agcm_target $stored_exe_dir
    cp $cpl_target $stored_exe_dir
    save $stored_exe_dir $stored_exe_dir
    rm -rf $stored_exe_dir
    cd -
fi

